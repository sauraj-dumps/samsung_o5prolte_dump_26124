#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:12648464:45dec1a0ed4e2cd43ab88d300d61c7920888880b; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/13540000.dwmmc0/by-name/BOOT:6557712:44e279927db9a7e6618e2c823d35b1b14fc728b9 \
          --target EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:12648464:45dec1a0ed4e2cd43ab88d300d61c7920888880b && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
